<?php 
require_once('lb_config.inc.php');
require_once('lb_includes/functions.inc.php');
if ($_GET['s'] == 'm') {
    $lb_mobile = 1;
}
else {
    $lb_mobile = 0;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo lb_load_config('lb_sub_title') . ' :: ' . lb_load_config('lb_main_title'); ?></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <?php if(!isset($_GET['p']) || $_GET['p'] == 1): ?>
    <meta http-equiv="refresh" content="30; URL=<?php echo $_SERVER['PHP_SELF'] . ($lb_mobile?'?s=m':''); ?>">
    <?php endif;?>
    <link href="<?php echo $lb_mobile?'mobile.css':'style.css'; ?>" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="script.js"></script>
</head>
<body>
    <div id="wrapper">
        <?php 
            //header
            echo lb_gen_header($lb_mobile);
        ?>
        <?php
            //main body
            // Process the page request.
            if($_GET['p'] <= 1 || !ctype_digit($_GET['p']) || !isset($_GET['p'])) {
                $page = 1;
            }
            else {
                $page = $_GET['p'];
            }
            echo lb_gen_body($lb_mobile, $page);
        ?>
        <?php
            //footer
            echo lb_gen_footer($lb_mobile);
        ?>
    </div>
</body>
</html>
