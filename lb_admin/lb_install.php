<?php
// Install the tables.
// Delete this file after installation!!!

require_once('../lb_config.inc.php');
require_once('../lb_includes/functions.inc.php');

$title = "Install LiveBlog";
echo lb_common_wrapper_start($title);
    // Create the necessery tables and create the new user.
if (isset($_POST['submitted'])) {
    mysql_connect(DB_HOST,DB_USER,DB_PASSWD) or die("<p>数据库连接失败。<br />请检查你是否设置了错误的用户名和密码。</p>");
    mysql_select_db(DB_NAME) or die("<p>数据库打开失败。<br />请检查你是否设置了错误的数据库名。</p>");
    $query = "CREATE TABLE lb_posts (
        pid INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
        uid SMALLINT UNSIGNED NOT NULL,
        sid SMALLINT UNSIGNED NOT NULL,
        time DATETIME NOT NULL,
        content TEXT)";
    mysql_query($query) or die("<p>lb_posts表创建失败！<br />请确保你有足够的权限创建表。</p>");
    $query = "CREATE TABLE lb_users (
        uid SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
        s_name VARCHAR(14) NOT NULL,
        f_name VARCHAR(50) NOT NULL,
        email VARCHAR(50) NOT NULL,
        password CHAR(40) NOT NULL,
        type TINYINT UNSIGNED NOT NULL)";
    mysql_query($query) or die("<p>lb_users表创建失败！<br />请确保你有足够的权限创建表。</p>");
    $query = "CREATE TABLE lb_sessions (
        sid INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
        session TINYTEXT NOT NULL,
        description TINYTEXT NOT NULL,
        time DATETIME)";
    mysql_query($query) or die("<p>lb_sessions表创建失败！<br />请确保你有足够的权限创建表。</p>");
	// Create Config table
	$query = "CREATE TABLE lb_config (
        sid SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
        conf_name VARCHAR(50) NOT NULL,
        conf_value VARCHAR(200) NOT NULL)";
	mysql_query($query) or die("<p>lb_config表创建失败！<br />请确保你有足够的权限创建表。</p>");
	// add config table rows.
	$query = "INSERT INTO lb_config SET conf_name='lb_main_title', conf_value='Live Blog (Codename: Probe)'";
	@mysql_query($query);
	$query = "INSERT INTO lb_config SET conf_name='lb_sub_title', conf_value='这里是直播标题'";
	@mysql_query($query);
	$query = "INSERT INTO lb_config SET conf_name='lb_server_tz', conf_value='0'";
	@mysql_query($query);
	$query = "INSERT INTO lb_config SET conf_name='lb_server_time_diff', conf_value='00:00:00'";
	@mysql_query($query);
	$query = "INSERT INTO lb_config SET conf_name='lb_place_tz', conf_value='-8'";
	@mysql_query($query);
	$query = "INSERT INTO lb_config SET conf_name='lb_local_tz', conf_value='+8'";
	@mysql_query($query);
	$query = "INSERT INTO lb_config SET conf_name='lb_enable_ff', conf_value='true'";
	@mysql_query($query);
	$query = "INSERT INTO lb_config SET conf_name='lb_ff_username', conf_value='test'";
	@mysql_query($query);
	$query = "INSERT INTO lb_config SET conf_name='lb_ff_password', conf_value='test'";
	@mysql_query($query);
	$query = "INSERT INTO lb_config SET conf_name='lb_ff_prefix', conf_value='[直播博客]: '";
	@mysql_query($query);
	// Add default user.
    $query = "INSERT INTO lb_users SET s_name='admin', 
        f_name='Administrator', 
        email='admin@admin.com', 
        password=SHA1('admin'),
        type=0";
    mysql_query($query) or die("<p>用户创建失败！<br />请确保你有足够的权限创建表。</p>");
    mysql_close();
    echo "<h1>安装完毕！</h1>
        <p>请删除install.php。然后使用<br />
        用户名：admin<br />密码：admin<br />登录LiveBlog。并尽快修改密码。<br />Enjoy!</p>";
}
else {
    // echo the db settings:
    $handler = $_SERVER['PHP_SELF'];
    echo "<h1>LiveBlog数据库安装向导</h1>";
    echo "请检查以下设置信息是否正确：" . 
        "<ul><li>数据库主机 : " . DB_HOST ."</li>" . 
        "<li>数据库名 : " . DB_NAME ."</li>" . 
        "<li>数据库用户 : " . DB_USER ."</li>" . 
        "<li>数据库密码: ****（已隐藏）</li></ul>" . 
        "<p>以上信息是否正确？如果不正确，请修改config.inc.php，然后刷新本页。<br />" . 
        "如果正确，点击“下一步”开始安装。</p>" . 
        "<form  action=\"$handler\" method=\"post\"><input type=\"submit\" value=\"下一步\"/>" . 
        "<input type=\"hidden\" value=\"1\" name=\"submitted\"/></form>";
}

echo lb_common_wrapper_end();

?>
