<?php
// 
// This is admin.php served as admin panel of liveblog.
//
require_once('../lb_includes/functions.inc.php');
if (isset($_COOKIE['author'])) {
    $title = "管理首页";
    echo lb_common_wrapper_start($title);
    // Welcome barner
    echo lb_welcome_barner();
    // Maybe some sort of Header like thing.
    //
    // Common Admin page links.
    echo lb_admin_navbar();
    //
    echo "<p>这里是管理首页。<br />请选择一个管理页进行设置，或开始<a href=\"lb_post.php\">发表日志</a>。</p>";
    //
    // Mabe a footer here.
    //
    //
    // Wrap the HTML page.
    echo lb_common_wrapper_end();
    
}
else {
    $url = lb_abs_url('login.php');
    header("Location: $url");
    exit();
}
?>