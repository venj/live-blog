<?php
// 
// This is admin.php served as admin panel of liveblog.
//
require_once('../lb_includes/functions.inc.php');
require_once('../lb_config.inc.php');

// Use a function to wrap the info barne.
// I'm not add this to functions.inc.php
// I have to control the size of functions.inc.php.
// This function will be only used here, right? 
function lb_uadmin_infobar() {
    // Handle newuser info database ops.
    if(isset($_POST['new_user_submit'])) {
        if($_POST['new_pswd_once'] !== $_POST['new_pswd_repeat']) {
            return lb_warn_div('warn', '两次输入的密码不匹配！');
        }
        elseif(trim($_POST['new_s_name']) == "" ||
               trim($_POST['new_f_name']) == "" ||
               trim($_POST['new_email']) == "" ||
               trim($_POST['new_pswd_once']) == "" ||
               trim($_POST['new_pswd_repeat']) == "") {
            return lb_warn_div('warn', '请输入完整的用户信息！');
        }
        else {
            // Temperarily set the type to 0, that is administrator.
            $new_s_name = trim($_POST['new_s_name']);
            $new_f_name = trim($_POST['new_f_name']);
            $new_email = trim($_POST['new_email']);
            $passwd = $_POST['new_pswd_once'];
            $query = "INSERT INTO lb_users SET s_name='$new_s_name',
                f_name='$new_f_name',email='$new_email',
                password=SHA1('$passwd')";
            $result = lb_db_query($query);
            if($result) {
                lb_db_close();
                return lb_warn_div('info', "用户“{$new_f_name}”新建成功！");
            }
            else {
                lb_db_close();
                return lb_warn_div('warn', '新建用户失败。 :( 请重试。');
            }
        }
    }
    // Handle password change info database ops.
    if(isset($_POST['change_pswd_submit'])) {
        if(trim($_POST['change_pswd_once']) == "" &&
               trim($_POST['change_pswd_repeat']) == "") {
            return lb_warn_div('warn', '密码不能为空！');
        }
        elseif($_POST['change_pswd_once'] !== $_POST['change_pswd_repeat']) {
            return lb_warn_div('warn', '两次输入的密码不匹配！');
        }
        else {
            $passwd = $_POST['change_pswd_once'];
            // Call a lb_current_uid()
            $uid = lb_current_uid();
            $query = "UPDATE lb_users SET password=SHA1('$passwd') WHERE uid='$uid'";
            $result = lb_db_query($query);
            if($result) {
                lb_db_close();
                return lb_warn_div('info', "密码修改成功！");
            }
            else {
                lb_db_close();
                return lb_warn_div('warn', '密码修改失败。 :( 请重试。');
            }
        }
    }
    // Handle user info change info database ops.
    if(isset($_POST['change_userinfo_submit'])) {
        $change_f_name = trim($_POST['change_f_name']);
        $change_email = trim($_POST['change_email']);
        if($change_f_name == "" && $change_email == "") {
            return lb_warn_div('warn', '修改用户信息时不能两项都留空！');
        }
        else {
            // Call a lb_current_uid()
            $uid = lb_current_uid();
            $query = "UPDATE lb_users SET ";
            if ($change_f_name != "") {
                $query .= "f_name='$change_f_name'";
                if ($change_email != "") {
                    $query .= ",";
                }
            }
            if ($change_email != "") {
                    $query .= "email='$change_email' ";
            }
            $query .= "WHERE uid='$uid'";
            $result = lb_db_query($query);
            if($result) {
                lb_db_close();
                return lb_warn_div('info', "用户信息修改成功！");
            }
            else {
                lb_db_close();
                return lb_warn_div('warn', '用户信息修改失败。 :( 请重试。');
            }
        }
    }
    // Handle user delete.
    if(isset($_GET['duid'])) {
        $uid = $_GET['duid'];
        $query = "DELETE FROM lb_users WHERE uid='$uid'";
        $result = lb_db_query($query);
        if($result) {
            lb_db_close();
            return lb_warn_div('info', "用户删除成功！");
        }
        else {
            lb_db_close();
            return lb_warn_div('warn', '用户删除失败。 :( 请重试。');
        }
    }
}

if (isset($_COOKIE['author'])) {
    // Author is certified. So Generate the User administration Page.
    $title = "用户管理";
    // HTML page header.
    echo lb_common_wrapper_start($title);
    // Welcome barner
    echo lb_welcome_barner();
    // Maybe some sort of Header like thing.
    //
    // Common Admin page links.
    echo lb_admin_navbar();
    // Info banner goes here.
    echo lb_uadmin_infobar();
    // Gen a list of users.
    echo lb_user_list(lb_current_uid());
    // Add user form
    echo lb_user_add();
    // User CSS to make some gaps here to differenciate the add new user form
    // and the personal info/password change forms.
    #DEBUG echo "<br />";
    echo lb_userinfo_change();
    echo lb_password_change();
    // Mabe a footer here.
    //
    //
    // Wrap the HTML page.
    echo lb_common_wrapper_end();
}
else {
    $url = lb_abs_url('login.php');
    header("Location: $url");
    exit();
}
?>