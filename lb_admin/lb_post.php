<?php
// 
// This is post.php served as liveblog post form.
//
require_once('../lb_includes/functions.inc.php');
require_once('../lb_config.inc.php');

if (isset($_COOKIE['author'])) {
    $title = "发表消息";
    // Handle the post form.
    if (isset($_POST['lb_submit'])) {
        $db = @mysql_connect(DB_HOST,DB_USER,DB_PASSWD);
        @mysql_select_db(DB_NAME,$db);
        $db_uid = lb_current_uid();
        $db_sid = lb_current_sid();
        $db_time = lb_proc_time();
        $ff_content = $_POST['lb_content'];
        $db_content = mysql_real_escape_string($_POST['lb_content'],$db);
        // Javascript will be used to avoid empty field.
        // But this will double ensure no empty content will be sent.
        if (strlen(trim($db_content)) == 0) {
            $warn_msg = lb_warn_div("warn", "你还没有写点东西就点“发表”了啊。 :(");
        }
        else {
            $query = "INSERT INTO lb_posts SET uid=$db_uid,
                    sid=$db_sid,time='$db_time',content='$db_content'";
            #DEBUG echo $query;
            $result = mysql_query($query,$db);
            if($result) {
                $warn_msg = lb_warn_div("info", "发表成功！");  
            }
            else {
                $warn_msg = lb_warn_div("warn", "发表失败！");
            }
            lb_db_close($db);
            // 启用饭否消息同步否？
            if(lb_load_config('lb_enable_ff')) lb_fanfou_autosplit("$ff_content");
        }
    }
    // Handle Update
    if(isset($_POST['lb_update']) && isset($_POST['update_pid']) && isset($_POST['update_content'])){
        @mysql_connect(DB_HOST,DB_USER,DB_PASSWD);
        @mysql_select_db(DB_NAME);
        $pid = $_POST['update_pid'];
        $ff_content = $_POST['update_content'];
        $content = mysql_real_escape_string($_POST['update_content']);
        $query = "UPDATE lb_posts SET content='$content' WHERE pid=$pid";
        $result = mysql_query($query);
        if($result) {
            $warn_msg = lb_warn_div("info", "更新成功！");  
        }
        else {
            $warn_msg = lb_warn_div("warn", "更新失败！");
        }
        @lb_db_close();
        if(lb_load_config('lb_enable_ff')) lb_fanfou_autosplit("$ff_content");
    }

    // Handle edit and delete
    //
    // This var controls the form output.
    $is_edit = 0;
    //
    if(isset($_GET['pid']) && isset($_GET['action'])) {
        $pid = $_GET['pid'];
        $action = $_GET['action'];
        if ($action == 'delete') {
            $query = "DELETE FROM lb_posts WHERE pid=$pid";
            $result = lb_db_query($query);
            if($result) {
                $warn_msg = lb_warn_div("info", "删除成功！");  
            }
            else {
                $warn_msg = lb_warn_div("warn", "删除失败！");
            }
            @lb_db_close();
        }
        elseif($action == 'edit') {
            $is_edit = 1;
            $query = "SELECT content FROM lb_posts WHERE pid=$pid";
            $result = lb_db_query($query);
            if($result) {
                list($content) = mysql_fetch_row($result);
                $handler = $_SERVER['PHP_SELF'];
                $update_form = <<<END
                <div div="post_form" class="form">
                    <form action="$handler" method="post">
                        <textarea name="update_content" rows="8" cols="24">$content</textarea><br />
                        <input type="hidden" name="update_pid" value="$pid" />
                        <input type="submit" name="lb_update" value="发表!" />
                    </form>
                    <p><a href="{$handler}?pid=$pid&action=delete">删除?</a></p>
                </div>
END;
            
            }
            else {
                $warn_msg = lb_warn_div("warn", "删除失败！");
            }
            @lb_db_close();
        }
    }

    // Generate the page.
    echo lb_common_wrapper_start($title);
    // Welcome barner
    echo lb_welcome_barner();
    // Maybe some sort of Header like thing.
    //
    // Common Admin page links.
    echo lb_admin_navbar();
    // Info barner
    echo $warn_msg;
    if ($is_edit) {
        echo $update_form;
    }
    else {
        echo lb_post_form();
    }
    echo lb_common_wrapper_end();
}
else {
    $url = lb_abs_url('lb_login.php');
    header("Location: $url");
    exit();
}
?>