<?php
// 
// This is admin.php served as admin panel of liveblog.
//
require_once('../lb_includes/functions.inc.php');
require_once('../lb_config.inc.php');
if (isset($_COOKIE['author'])) {
    $title = "直播管理";
    echo lb_common_wrapper_start($title);
    // Welcome barner
    echo lb_welcome_barner();
    // Maybe some sort of Header like thing.
    //
    // Common Admin page links.
    echo lb_admin_navbar();
    
    echo lb_edit_sessions();
    echo lb_list_sessions();
    echo lb_create_session_form();
    //
    // Mabe a footer here.
    //
    //
    // Wrap the HTML page.
    echo lb_common_wrapper_end();
    
}
else {
    $url = lb_abs_url('login.php');
    header("Location: $url");
    exit();
}

function lb_list_sessions() {
    $html = "<div class=\"lb_form\">
    <h3>管理直播</h3>
    <table>
    <tr><td>直播名称</td><td>直播描述</td><td>直播时间</td></tr>";
    $query = "SELECT session,description,time FROM lb_sessions ORDER BY time DESC";
    $result = lb_db_query($query);
    if($result) {
        if(mysql_affected_rows == 0) {
            $html .= "<tr><td colspan=\"3\">还没有任何直播，你可以通过下面的表单新建一个直播。 :) </td></tr>";
        }
        else {
            while(list($session,$description,$time) = mysql_fetch_row($result)) {
                $html .= "<tr><td>$session</td><td>$description</td><td>$time</td></tr>";
            }
        }
    }
    else {
        $html .= "<tr><td colspan=\"3\">数据库查询出错啦。 :( <br />请检查直播博客的数据库设置。  </td></tr>";
    }
    $html .= "</table>";
    return $html;
}
function lb_create_session_form() {
    $handler = $_SERVER['PHP_SELF'];
    $html =<<<END
    <div class="lb_form">
    <h3>新建直播</h3>
        <form method="post" action="$handler" id="lb_session_form" name="lb_session_form">
            <table>
                <tr><td>直播主题</td><td><input type="text" name="lb_sessiom_topic" value=""/></td></tr>
                <tr><td>直播简介</td><td><textarea name="lb_session_desc"></textarea></td></tr>
                <tr><td colspan="2"><input type="submit" name="change_userinfo_submit" value="添加"/></td></tr>
            </table>
        </form>
    </div>
END;
    
    return $html;
}
function lb_edit_sessions() {
    if(isset($_POST['submit'])) {
        // Later.
    }
}
?>