<?php
// 
// This is admin.php served as admin panel of liveblog.
//
require_once('../lb_includes/functions.inc.php');
require_once('../lb_config.inc.php');
if (isset($_COOKIE['author'])) {
    $title = "系统配置";
    echo lb_common_wrapper_start($title);
    // Welcome barner
    echo lb_welcome_barner();
    // Maybe some sort of Header like thing.
    //
    // Common Admin page links.
    echo lb_admin_navbar();
    //
    echo lb_config_update();
    //
    // Mabe a footer here.
    //
    //
    // Wrap the HTML page.
    echo lb_common_wrapper_end();
    
}
else {
    $url = lb_abs_url('login.php');
    header("Location: $url");
    exit();
}

function lb_sys_config_form() {
$handler = $_SERVER['PHP_SELF'];
$query = "SELECT conf_name,conf_value FROM lb_config";
$result = lb_db_query($query);
if($result) {
	while(list($name,$value) = mysql_fetch_row($result)) {
		$$name = $value;
	}
	if($lb_enable_ff == "true") {
		$checked = 'checked';
	}
	else {
		$checked = '';
	}
	lb_db_close();
}
else {
	lb_db_close();
	return lb_warn_div('warn', '设置加载失败！请刷新本页。');
};
$html = <<<END
<form action="$handler" method="post" enctype="multipart/form-data">
    <table id="lb_setting_table">
      <tr>
        <td>直播博客名</td>
        <td><input name="lb_main_title" type="text" value="$lb_main_title" size="25" maxlength="50" /></td>
        <td>博客副标题</td>
        <td><input name="lb_sub_title" type="text" value="$lb_sub_title" size="25" maxlength="100" /></td>
      </tr>
      <tr>
        <td>服务器时区</td>
        <td><input name="lb_server_tz" type="text" size="4" maxlength="4" value="$lb_server_tz"/></td>
        <td>服务器时间偏差</td>
        <td><input name="lb_server_time_diff" type="text" size="10" maxlength="25" value="$lb_server_time_diff"/></td>
      </tr>
      <tr>
        <td>直播地时区</td>
        <td><input name="lb_place_tz" type="text" size="4" maxlength="4" value="$lb_place_tz"/></td>
        <td>本地时区</td>
        <td><input name="lb_local_tz" type="text" size="4" maxlength="4" value="$lb_local_tz"/></td>
      </tr>
	  <tr>
        <td colspan="4"><input name="lb_enable_ff[]" type="checkbox" value="enable" $checked/>是否启用饭否同步？</td>
      </tr>
      <tr>
        <td>饭否用户名</td>
        <td><input name="lb_ff_username" type="text" size="25" maxlength="25" value="$lb_ff_username"/></td>
        <td>饭否密码</td>
        <td><input name="lb_ff_password" type="password" size="25" maxlength="25" value="$lb_ff_password"/></td>
      </tr>
      <tr>
        <td>饭否同步消息前缀</td>
        <td colspan="3"><input name="lb_ff_prefix" type="text" value="$lb_ff_prefix" /></td>
      </tr>
    </table>
	<p><input name="submit" type="submit" value="更新设置" /></p>
  </form>
END;

return $html;
}

function lb_config_update() {
	if(isset($_POST['submit'])) {
		mysql_connect(DB_HOST,DB_USER,DB_PASSWD) or die("<p>数据库连接失败。<br />请检查你是否设置了错误的用户名和密码。</p>");
		mysql_select_db(DB_NAME) or die("<p>数据库打开失败。<br />请检查你是否设置了错误的数据库名。</p>");
		$lb_main_title = mysql_real_escape_string($_POST['lb_main_title']);
		$lb_sub_title = mysql_real_escape_string($_POST['lb_sub_title']);
		$lb_server_tz = $_POST['lb_server_tz'];
		$lb_server_time_diff = $_POST['lb_server_time_diff'];
		$lb_place_tz = $_POST['lb_place_tz'];
		$lb_local_tz = $_POST['lb_local_tz'];
		$lb_enable_ff = ($_POST['lb_enable_ff'][0] == 'enable'?'true':'false');
		$lb_ff_username = $_POST['lb_ff_username'];
		$lb_ff_password = $_POST['lb_ff_password'];
		$lb_ff_prefix = mysql_real_escape_string($_POST['lb_ff_prefix']);
		// update config table rows.
		$query = "UPDATE lb_config SET conf_value='$lb_main_title' WHERE conf_name='lb_main_title'";
		@mysql_query($query);
		$query = "UPDATE lb_config SET conf_value='$lb_sub_title' WHERE conf_name='lb_sub_title'";
		@mysql_query($query);
		$query = "UPDATE lb_config SET conf_value='$lb_server_tz' WHERE conf_name='lb_server_tz'";
		@mysql_query($query);
		$query = "UPDATE lb_config SET conf_value='$lb_server_time_diff' WHERE conf_name='lb_server_time_diff'";
		@mysql_query($query);
		$query = "UPDATE lb_config SET conf_value='$lb_place_tz' WHERE conf_name='lb_place_tz'";
		@mysql_query($query);
		$query = "UPDATE lb_config SET conf_value='$lb_local_tz' WHERE conf_name='lb_local_tz'";
		@mysql_query($query);
		$query = "UPDATE lb_config SET conf_value='$lb_enable_ff' WHERE conf_name='lb_enable_ff'";
		@mysql_query($query);
		$query = "UPDATE lb_config SET conf_value='$lb_ff_username' WHERE conf_name='lb_ff_username'";
		@mysql_query($query);
		$query = "UPDATE lb_config SET conf_value='$lb_ff_password' WHERE conf_name='lb_ff_password'";
		@mysql_query($query);
		$query = "UPDATE lb_config SET conf_value='$lb_ff_prefix' WHERE conf_name='lb_ff_prefix'";
		@mysql_query($query);
		mysql_close();
		$html = lb_warn_div('info', '设置已更新！');
		$html .= lb_sys_config_form();
		return $html;
		
	}
	else {
		return lb_sys_config_form();
	}
}

?>